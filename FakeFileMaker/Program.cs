﻿using System;
using System.IO;

namespace FakeFileMaker
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.Title = "Fake File Generator by </Sir.Arash>";
            Console.WriteLine();
            Console.WriteLine("  =============================================", ConsoleColor.Yellow, ConsoleColor.Black);
            Console.WriteLine("  ============ Fake File Generator ============", ConsoleColor.Yellow, ConsoleColor.Black);
            Console.WriteLine("  ==============  Version: 1.0  ===============", ConsoleColor.Yellow, ConsoleColor.Black);
            Console.WriteLine("  ========== Developer: </Sir.Arash> ==========", ConsoleColor.Yellow, ConsoleColor.Black);
            Console.WriteLine("  =============================================", ConsoleColor.Yellow, ConsoleColor.Black);
            Console.WriteLine();

            try
            {
                Console.Write("\r\n [?] Enter output path: ", ConsoleColor.Cyan, ConsoleColor.Black);
                string text = Console.ReadLine();
                Console.Write("\r\n [?] Enter size of file (MB): ", ConsoleColor.Cyan, ConsoleColor.Black);
                long length = long.Parse(Console.ReadLine()) * 1048576L;
                Console.Write("\r\n [?] Enter creation date: ", ConsoleColor.Cyan, ConsoleColor.Black);
                DateTime dateTime = DateTime.Parse(Console.ReadLine());
                FileStream fileStream = new FileStream(text, FileMode.Create, FileAccess.Write, FileShare.None);
                fileStream.SetLength(length);
                fileStream.Close();
                File.SetCreationTime(text, dateTime);
                File.SetLastWriteTime(text, dateTime);
                Console.WriteLine("\r\n [!] File successfully generated, file path: " + text, ConsoleColor.Green, ConsoleColor.Black);
            }
            catch (Exception ex)
            {
                Console.WriteLine("\r\n [!] An error occurred: " + ex.Message, ConsoleColor.Red, ConsoleColor.Black);
            }
            Console.Write("\r\n [>>] Press any key to close . . . ");
            Console.ReadKey(true);
        }
    }
}
